# encoding: utf-8
from squirro.sdk import PipeletV1
class KeywordsPipelet(PipeletV1):
    """Adds all the keywords from the config statically to the item.
    Used in data loaders to e.g. add a `Type` field.
    Example:
        "KeywordsPipelet":{
            "file_location":"../../common/enrich/keywords/keywords.py",
            "config": {
                "Type": "Fund",
            }
        },
    """
    def __init__(self, config):
        self._keywords = config
    def consume(self, item):
        """Process a single item or sub-item.
        """
        for kw, values in self._keywords.iteritems():
            if not isinstance(values, list):
                values = [values]
            item.setdefault('keywords', {})[kw] = values
        return item