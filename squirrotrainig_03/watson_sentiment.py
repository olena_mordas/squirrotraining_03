"""
This pipelet enriches each incoming items with Watsons Sentiment Analyzer.
"""
from squirro.sdk import PipeletV1, require
from hashlib import sha256
from datetime import datetime
import requests
import json
import os
import errno
API_URL = 'https://gateway.watsonplatform.net/natural-language-understanding/api/v1/analyze?version=2017-02-27'
FACETS = ['sentiment_version', 'Sentiment', 'PositiveSentiment', 
'NegativeSentiment', 'SentimentScore']
@require('log')
class WatsonSentimentPipelet(PipeletV1):
    def __init__(self, config):
        # If an API key is required
        if not 'url' in config:
            config['url'] = API_URL
        # If an API key is required
        if not 'username' in config:
            raise ValueError('Missing Watson username')
        if not 'password' in config:
            raise ValueError('Missing Watson password')
        if not 'sentiment_version' in config:
            raise ValueError('Missing Watson tone_version')
        self.config = config
        self.headers = {
            'Content-Type': 'application/json'
        }
        self.auth = ( config['username'], config['password'] )
    def consume(self, item):
        self._enrich(item)
        return item
    def _enrich(self, item):
        #ensure keywords is always set
        if item.get('keywords') is None:
            item['keywords'] = {}
        else:
            #ensuring the slate is clean
            for facet in FACETS:
                if item['keywords'].get(facet):
                    del item['keywords'][facet]
        #set the latest version for later enrichment reruns
        item['keywords']['sentiment_version'] = [self.config['sentiment_version']]



        body = item.get('body', '')
        #short bodies are not worth analyzing
        if len(body) < 50:
            item['keywords']['PositiveSentiment'] = [0]
            item['keywords']['NegativeSentiment'] = [0]
            item['keywords']['SentimentScore'] = [0]
        else:
            data = {
                "html": item.get('body', ''),
                "features": {
                    "sentiment": {"document": True}
                },
                "clean": True,
                "fallback_to_raw": True,
                "return_analyzed_text": False
            }
            #run the request via bluemix
            response = self.get_response(url=self.config['url'], http='POST',
                                         headers=self.headers,
                                         auth=self.auth, data=json.dumps(data))
            #extract data from response
            sentiment = response.get('data', {}).get('sentiment', {})
            document_sentiment = sentiment.get('document', {})
            #set the facet with the positive or negative label
            document_label = document_sentiment.get('label', 'not defined')
            if document_label == 'positive' or document_label == 'negative':
                item['keywords']['Sentiment'] = [document_label]
            #get the percent score of the sentiment
            document_score = int(document_sentiment.get('score', 0)*100)
            document_positive_score = 0
            document_negative_score = 0
            if document_label == 'positive':
                document_positive_score = document_score
                item['keywords']['SentimentScore'] = [document_score]
            elif document_label == 'negative':
                document_negative_score = document_score
                item['keywords']['SentimentScore'] = [10]
            else:
                document_positive_score = document_score
                item['keywords']['SentimentScore'] = [10]
            #set the score facets
            item['keywords']['PositiveSentiment'] = [document_positive_score]
            item['keywords']['NegativeSentiment'] = [abs(document_negative_score)]
        return item
    # API Response caching framework
    # The main method, This is what is called to get a remote cached API response
    def get_response(self, url, cache_location='/tmp/sentiment_cache/',
                     store_errors=False, timeout=None, http='GET', headers=None,



                     auth=None, data=None):
        # Check the cache first, then if no cache, go to the API to get the folder data
        response_data = self._lookup_cache(url, headers, data, cache_location, timeout)
        # Cache Miss
        if not response_data:
            # Make API Request
            response = self.get_web_response(url, http, headers, auth, data)
            if (store_errors) or (int(response['status_code']) < 300):
                try:
                    # Write to cache after successful API response
                    self._write_cache(url, headers, data, response,
                                      cache_location)
                except ValueError:
                    # If writing the first time fails, it is useful to try
                    # a second time with the encoding set
                    try:
                        self._write_cache(url, headers, data,
                                          response.encode('utf8'),
                                          cache_location)
                    except UnicodeEncodeError:
                        self.log.error('Error writing the response to the cache')
        # Cache Hit
        else:
            # work with the res_data as if it were the API response
            response = response_data
        return response
    def get_web_response(self, url, http, headers, auth, data):
        if http in ['GET', 'Get', 'get']:
            response = requests.get(url, headers=headers, auth=auth, data=data)
        elif http in ['POST', 'Post', 'post']:
            response =  requests.post(url, headers=headers, auth=auth, 
data=data)
        else:
            return None
        try:
            response_data = json.loads(response.text)
        except Exception:
            response_data = response.text
        response_status = response.status_code
        response_dict = {
            "data": response_data,
            "status_code": response_status



        }
        return response_dict
    def _lookup_cache(self, url, headers, data, cache_location, timeout):
        file_path = self._cache_file_name(url, headers, data, cache_location)
        path = os.path.dirname(file_path)
        try:
            os.makedirs(path)
        except OSError as ex:
            if ex.errno != errno.EEXIST:
                self.log.error('Could not create cache folder %s', path)
                return None
        try:
            # Try to get the last modified date of the file
            if (self.get_cache_age(file_path) < timeout) or (not timeout):
                with open(file_path, 'rb') as f:
                    response = json.load(f)['response']
                    return response
            else:
                return None
        except Exception:
            return None
    def get_cache_age(self, file_path):
        modified_time = self.get_modified_time(file_path)
        current_time = datetime.now()
        cache_age = current_time - modified_time
        age_days = cache_age.days
        age_seconds = cache_age.seconds
        # Calculate the total number of second elapsed
        total_age_seconds = age_seconds + (86400 * age_days)
        # Convert the total age in seconds into hours (rounded down)
        total_age_hours = total_age_seconds // 3600
        return total_age_hours
    def get_modified_time(self, file_path):
        time_stamp = os.path.getmtime(file_path)
        return datetime.fromtimestamp(time_stamp)
    def _write_cache(self, url, headers, data, response, cache_location):
        file_path = self._cache_file_name(url, headers, data, cache_location)
        json_object = {'cache-key': url, 'response': response}



        with open(file_path, 'wb') as f:
            json.dump(json_object, f)
    def _cache_file_name(self, url, headers, data, cache_location):
        digest_body = str({
            "request": repr(url),
            "headers": repr(headers),
            "data": repr(data)
            })
        digest = sha256(digest_body).hexdigest()
        path = os.path.join(cache_location, digest[:2], digest[2:4], 
digest[4:6])
        file_path = path + '/' + digest + '.json'
        return file_path